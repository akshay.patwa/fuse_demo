import { Routes } from '@angular/router';
import { LoginComponent } from './main/pages/authentication/login/login.component';

export const appRoutes: Routes = [
    {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full'
    },
    {
      path: 'pages',
      loadChildren: './main/pages/pages.module#PagesModule',
    },
    {
        path      : '**',
        redirectTo: 'sample'
    }
];